# -*- coding: utf-8 -*-
import logging
from odoo import api, models, fields
import psycopg2

logger = logging.getLogger(__name__)

LARGE_OBJECT_LOCATION = 'postgresql:lobject'


class IrAttachment(models.Model):
    """Provide storage as PostgreSQL large objects of attachements with filestore location ``%r``.

    Works by overriding the storage handling methods of ``ir.attachment``, as intended by the
    default implementation. The overrides call :funct:`super`, so that this is transparent
    for other locations.
    """ % LARGE_OBJECT_LOCATION

    _name = 'ir.attachment'
    _inherit = 'ir.attachment'

    def lobject(self, *args):
        cr = self.env.cr
        return cr._cnx.lobject(*args)

    def _file_write(self, value, checksum):
        """Write the content in a newly created large object.

        :param value: base64 encoded payload
        :returns str: object id (will be considered the file storage name)
        """
        location = self._storage()
        if location != LARGE_OBJECT_LOCATION:
            return super(IrAttachment, self)._file_write(value, checksum)

        lobj = self.lobject(0, 'wb')  # oid=0 means creation
        lobj.write(value.decode('base64'))
        oid = lobj.oid
        return str(oid)

    def _lobject_delete(self, fname):
        """Delete the large object.

        :param fname: file storage name, must be the oid as a string.
        """
        try:
            oid = long(fname)
        except Exception:
            # it means there is a legacy attachment
            self._file_delete(fname)
        else:
            self.lobject(oid, 'rb').unlink()

    def _lobject_read(self, fname, bin_size):
        """Read the large object, base64 encoded.

        :param fname: file storage name, must be the oid as a string.
        """
        lobj = self.lobject(long(fname), 'rb')
        if bin_size:
            return lobj.seek(0, 2)
        return lobj.read().encode('base64')  # GR TODO it must be possible to read-encode in chunks

    @api.depends('store_fname', 'db_datas')
    def _compute_datas(self):
        bin_size = self._context.get('bin_size')
        for attach in self:
            try:
                attach.datas = self._lobject_read(attach.store_fname, bin_size)
            except (psycopg2.OperationalError, ValueError):
                if attach.store_fname:
                    attach.datas = self._file_read(attach.store_fname, bin_size)
                else:
                    attach.datas = attach.db_datas

    def _inverse_datas(self):
        location = self._storage()
        for attach in self:
            # compute the fields that depend on datas
            value = attach.datas
            bin_data = value and value.decode('base64') or ''
            file_size = len(bin_data)
            vals = {
                'file_size': file_size,
                'checksum': self._compute_checksum(bin_data),
                'index_content': self._index(bin_data, attach.datas_fname, attach.mimetype),
                'store_fname': False,
                'db_datas': value,
            }
            if value and location != 'db':
                # save it to the filestore
                vals['store_fname'] = self._file_write(value, vals['checksum'])
                vals['db_datas'] = False

            # take current location in filestore to possibly garbage-collect it
            fname = attach.store_fname
            # write as superuser, as user probably does not have write access
            super(IrAttachment, attach.sudo()).write(vals)

            # ugly, but the only way to update the file_size
            self.env.cr.execute("""
                UPDATE %s SET file_size = %s WHERE id %s %s
            """ % (self._table, file_size, 'in' if len(self._ids) > 1 else '=',
                   tuple(self._ids) if len(self._ids) > 1 else self.id))

            if fname:
                try:
                    self._lobject_delete(fname)
                except (psycopg2.OperationalError, ValueError):
                    self._file_delete(fname)

    datas = fields.Binary(
        string='File Content', compute="_compute_datas", inverse="_inverse_datas"
    )

    @api.model
    def create(self, values, context=None):
        # ugly but correct a base module
        if 'file_size' in values:
            del values['file_size']
        return super(IrAttachment, self).create(values)
