# -*- python -*-
"""This is a template upgrade script.

The purpose is both to cover the most common use-case (updating all modules)
and to provide an example of how this works.
"""


def run(session, logger):
    """Update all modules."""
    if session.is_initialization:
        session.install_modules(['attachment_large_object'])
        parameter = session.registry('ir.config_parameter')
        logger.info('Setup paramter to use advanced attachment')
        parameter.set_param(
            session.cr, session.uid, 'ir_attachment.location',
            'postgresql:lobject')

        return

    logger.info("Default upgrade procedure : updating all modules.")
    session.update_modules(['all'])
