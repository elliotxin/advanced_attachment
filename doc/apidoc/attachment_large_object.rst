attachment_large_object package
===============================

Subpackages
-----------

.. toctree::

    attachment_large_object.tests

Submodules
----------

attachment_large_object.__openerp__ module
------------------------------------------

.. automodule:: openerp.addons.attachment_large_object.__openerp__
    :members:
    :undoc-members:
    :show-inheritance:

attachment_large_object.ir_attachment module
--------------------------------------------

.. automodule:: openerp.addons.attachment_large_object.ir_attachment
    :members:
    :undoc-members:
    :private-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: openerp.addons.attachment_large_object
    :members:
    :undoc-members:
    :show-inheritance:
