attachment_large_object.tests package
=====================================

Submodules
----------

attachment_large_object.tests.test_attachment module
----------------------------------------------------

.. automodule:: openerp.addons.attachment_large_object.tests.test_attachment
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: openerp.addons.attachment_large_object.tests
    :members:
    :undoc-members:
    :show-inheritance:
