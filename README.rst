Advanced attachments
====================

These addons enhance Odoo’s native attachment system with several interesting features.

Documentation: `advanced_attachment <http://docs.anybox.fr/advanced_attachment/current/>`_

Blog article: `PostgreSQL large object storage for Odoo <https://anybox.fr/blog/postgresql-large-object-storage-for-odoo>`_
