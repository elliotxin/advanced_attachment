# -*- coding: utf-8 -*-
import os
import sys
import logging
from argparse import ArgumentParser
from openerp.tools import config

logger = logging.getLogger(os.path.split(sys.argv[0])[1])

DEF_TRANS_NUMBER = 50


def run(session):
    """Script to convert any format attachment to advanced attachment
    """

    parser = ArgumentParser(epilog="""
        This script will help you to convert existing attachment saved on file
        system or in the database to large object format.
    """)

    parser = ArgumentParser(
        description="Script to upgrade odoo attachment to "
                    "large object attachment.")
    parser.add_argument('-d', '--database',
                        help="Database to work on")
    parser.add_argument('-N', '--transaction-number', default=DEF_TRANS_NUMBER,
                        help="Tell how often commit. "
                             "Default, commit every %s attachments" %
                        DEF_TRANS_NUMBER, type=int)
    parser.add_argument('-F', '--max-file-size',
                        help="Max file size to upgrade (in octets)", type=int)
    parser.add_argument('-p', '--odoo-data-dir',
                        help="Odoo filestore directory, default %s." %
                        config.get('data_dir', ''))
    parser.add_argument('-s', '--simulate', action='store_true',
                        help="Simulation mode: "
                             "just logs actions but does not perform.")
    parser.add_argument('-f', '--force', action='store_true',
                        help="Do not raise if attachment data is missing,"
                             "carry on silently")

    parsed_args = parser.parse_args()

    if parsed_args.odoo_data_dir:
        config['data_dir'] = parsed_args.odoo_data_dir

    logger.info("Filestore directory: %s", config.get('data_dir'))
    session.open(db=parsed_args.database)

    # now migrate attachments

    # we use an sql query because odoo add filter in search method
    # but we want all the line
    query = "SELECT id FROM ir_attachment WHERE type='binary'"
    params = []
    max_size = parsed_args.max_file_size
    if max_size is not None:
        query += " AND file_size < %s"
        params.append(max_size)

    session.cr.execute(query, params)
    atts_ids = [x[0] for x in session.cr.fetchall()]

    atts_obj = session.env['ir.attachment']
    atts = atts_obj.browse(atts_ids)
    count = 0
    no_data = 0
    for att in atts:
        if att.store_fname:
            datas = att._file_read(att.store_fname)
        else:
            datas = att.db_datas
        if not datas:
            if parsed_args.force:
                no_data += 1
                logger.warn("This attachment won't be converted (id=%d)"
                            "data is missing",
                            att.id)
                continue
            else:
                session.cr.rollback()
                logger.error("There is missing data on row: %d", att.id)
                raise
        if parsed_args.simulate:
            logger.debug("Simulate mode, attachment id:%d would be updated",
                         att.id)
        else:
            logger.debug("Attachement id:%s is going to be updated.", att.id)
            att.write({'datas': datas, })
        count = count + 1
        if (count % parsed_args.transaction_number) == 0:
            if not parsed_args.simulate:
                session.cr.commit()
                logger.info("Commit last %d attachments",
                            parsed_args.transaction_number)
            else:
                session.cr.rollback()
                logger.warn("Work not commited as supplied by -s, "
                            "%d attachments rollbacked",
                            parsed_args.transaction_number)

    if not parsed_args.simulate:
        session.cr.commit()
        logger.info("Last commit, %s attachments converted (and %s missing).",
                    count, no_data)
    else:
        session.cr.rollback()
        logger.warn("Work not commited as supplied by -s. "
                    "%s attachment converted (and %s missing)", count, no_data)
