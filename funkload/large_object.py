import os
from webunit.utility import Upload
from anybox.funkload.openerp import OpenERPTestCase

DATA_DIR = os.path.join(os.path.dirname(__file__), 'upload_src')


class LargeObjectPreparation(OpenERPTestCase):

    def setUp(self):
        super(LargeObjectPreparation, self).setUp()
        self.login('admin', 'admin')

    def test_create_source_files(self):
        """Create files to be uploaded."""
        if os.path.exists(DATA_DIR):
            return
        os.mkdir(DATA_DIR)
        with open(os.path.join(DATA_DIR, '10mb'), 'wb') as f:
            f.write('blba7gbdaq' * 2**20)

    def test_ensure_users(self):
        """Ensure that users listed in the credential server do exist."""

        self.ensure_credential_server_users()

    def test_switch_lobject(self):
        self.model('ir.config_parameter').set_param('ir_attachment.location',
                                                    'postgresql:lobject')

    def test_give_rights(self):
        self.model('ir.model.access').create(dict(
            name="Open rights on res.users for attachment benchmark",
            model_id=self.ref('ir.model', 'base', 'model_res_users'),
            group_id=self.ref('res.groups', 'base', 'group_user'),
            perm_write=1, perm_read=1, perm_create=0, perm_unlink=0))


class LargeObjectTestCase(OpenERPTestCase):

    has_nomenclature_data = False

    def setUp(self):
        super(LargeObjectTestCase, self).setUp()
        self.web_login('admin', 'admin')  # TODO random user
        self.target_model = 'res.users'

    def test_attachment(self):
        self.login_as_group('base.group_user')
        self.post(self.server_url + '/web/binary/upload_attachment',
                  params=[['callback', '""'],
                          ['model', 'res.users'],
                          ['id', str(self.uid)],
                          ['session_id', self.web_session_id],
                          ['ufile', Upload(os.path.join(DATA_DIR, '10mb'))]])
