#==============================================================================
#                                                                             =
#    Desable the menu attachment
#    Copyright (C) 2014 Anybox (<http://anybox.fr>)
#                                                                             =
#    This file is a part of hr_bdes
#                                                                             =
#    hr_bdes is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License v3 or later
#    as published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#                                                                             =
#    hr_bdes is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License v3 or later for more details.
#                                                                             =
#    You should have received a copy of the GNU Affero General Public License
#    v3 or later along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#                                                                             =
#==============================================================================
{
    'name': 'document_desable_attachment',
    'shortdesc': 'Desable attachment',
    'version': '0.1',
    'summary': '',
    'category': 'Document',
    'description': """
    """,
    'author': 'Anybox',
    'website': 'http://anybox.fr',
    'depends': [
        'base',
        'web',
        'document',
    ],
    'data': [
        'template.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
    'license': 'AGPL-3',
}
