openerp.document_desable_attachment = function(instance) {
    instance.web.Sidebar.include({
        init : function(){ 
            this._super.apply(this, arguments);                                 
            var self = this;
            if (this.getParent().view_type == "form"){ 
                _(this.sections).each(function(section) {
                    if (section.name === 'files') {
                        var index = self.sections.indexOf(section);
                        self.sections.splice(index, 1);
                    }
                });
            }
        },
    })
}
