from setuptools import setup, find_packages

version = '1.0'

setup(name='advanced.attachment.scripts',
      version=version,
      description="Odoo Advanced attachment scripts",
      long_description=""" """,
      classifiers=[],
      author=u'Pierre Verkest',
      author_email='pverkest@anybox.fr',
      url='',
      license='',
      namespace_packages=['scripts'],
      packages=find_packages('.',
                             exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          # -*- Extra requirements: -*-
          'setuptools',
          'argparse',
      ],
      entry_points="""
      [console_scripts]
      convert_attachment=scripts.attachment.convert_attachment:run
      """,
      )
